import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import styles from './client/components/styles';
import countries from './client/components/countries';
import months from './client/components/months';
import years from './client/components/years';
import Validation from './client/components/validation';
import MenuItem from '@material-ui/core/MenuItem';
import _ from 'underscore';
import './client/styles/dist/App.css';

class App extends Component {
    constructor (props) {
        super(props);

        this.state = {
            country: '',
            region: '',
            billingCountry: '',
            billingRegion: '',
            month: '',
            year: '',
            gender: '',
            creditCardNumber: '',
            emailAddressError: '',
            creditCardNumberError: '',
            passwordError: '',
            firstNameError: '',
            lastNameError: '',
            streetAddressError: '',
            zipCodeError: '',
            countryError: '',
            regionError: '',
            cityError: '',
            billingStreetAddressError: '',
            billingZipCodeError: '',
            billingRegionError: '',
            billingCityError: '',
            billingCountryError: '',
            securityCode: '',
            securityCodeError: '',
            securityCodeMobile: '',
            securityCodeMobileError: '',
            monthError: '',
            yearError: '',
            genderError: '',
            disableRegion: true,
            disableBillingRegion: true,
            billingAddressCheck: true,
            creditCheck: true,
            showBillingAddress: false,
        };
    }

    selectHandler (e) {
        this.setState({
            [e.target.name]: e.target.value,
        });

        if (e.target.name === 'country') {
            this.setState({
                disableRegion: !(e.target.value && e.target.value.length),
            });
        } else if (e.target.name === 'billingCountry') {
            this.setState({
                disableBillingRegion: !(e.target.value && e.target.value.length),
            });
        }
    }

    validateSelect (e) {
        Validation.SelectValidation(e, this);
    }

    validate (e) {
        Validation.InputValidation(e, this);
    }
    
    validateCreditCard (e) {
        Validation.CreditCardValidation(e, this);
    }

    validateSecurityCode (e) {
        Validation.SecurityCodeValidation(e, this);
    }

    handleCheck = name => event => {
        this.setState({
            [name]: event.target.checked,
            showBillingAddress: name === 'billingAddressCheck' && !event.target.checked
        });
    };

    checkGender = event => {
        this.setState({ gender: event.target.value });
    }

    checkGenderByImage = event => {
        this.setState({ gender: event.target.name });
    }

    register() {
        console.log('gago')
    }

    render() {
        let self = this;
        let regions = (self.state.country && (_.findWhere(countries, {country: self.state.country}))['states']) || [];
        let billingRegions = (self.state.billingCountry && (_.findWhere(countries, {country: self.state.billingCountry}))['states']) || [];
        const { classes } = this.props;

        return (
            <div className="container">
                <div className="row">
                    <div className="topHeader">
                        <img className="logo" src={require("./client/images/logo.png")} alt="logo" />
                    </div>

                    <div className="col-xs-12 col-sm-12 pageBody">
                        <div>
                            <div className="col-xs-12 col-sm-4">
                                <div className="row priceInfoContainer">
                                    <div className="col-xs-12 priceInfoHolder">
                                        <div className="col-xs-12 monthlySubscriptionMobile onlyMobile">
                                            <h1 className='noMargin'>MONTHLY SUBSCRIPTION</h1>
                                            <h5 style={{color: '#ff67a1'}}>Billed monthly. Renews automatically, cancel any time. Free shipping.</h5>
                                        </div>

                                        <div className="col-xs-12 imageContainer">
                                            <img className="cosmetic" src={require("./client/images/mask.png")} alt="cosmetic" />
                                        </div>

                                        <div className="col-xs-12 cosmeticsInfo">
                                            <div className="row">
                                                <div className="col-xs-6">
                                                    <p className="text-left">Monthly sunbscription</p>
                                                </div>
                                                <div className="col-xs-6">
                                                    <p className="text-right">$14.95</p>
                                                </div>
                                            </div>

                                            <div className="row">
                                                <div className="col-xs-6">
                                                    <p className="text-left">Shipping</p>
                                                </div>
                                                <div className="col-xs-6">
                                                    <p className="text-right">FREE</p>
                                                </div>
                                            </div>

                                            <div className="row">
                                                <div className="col-xs-6">
                                                    <p className="text-left">Tax</p>
                                                </div>
                                                <div className="col-xs-6">
                                                    <p className="text-right">$2.35</p>
                                                </div>
                                            </div>

                                            <div className="row">
                                                <div className="col-xs-6">
                                                    <p className="text-left">Discount</p>
                                                </div>
                                                <div className="col-xs-6">
                                                    <p className="text-right" style={{color: '#fe8fb7'}}>-$5</p>
                                                </div>
                                            </div>

                                            <div className="row">
                                                <div className="col-xs-6">
                                                    <p className="text-left">Credit (Balance $100)</p>
                                                </div>
                                                <div className="col-xs-6">
                                                    <p className="text-right">
                                                        $50
                                                        <Checkbox
                                                            checked={this.state.creditCheck}
                                                            onChange={this.handleCheck('creditCheck')}
                                                            value="creditCheck"
                                                            size={50}
                                                            classes={{
                                                                root: classes.creditRoot,
                                                                checked: classes.checked,
                                                            }}
                                                        />
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-xs-12 cosmeticsInfo">
                                            <div className="row">
                                                <div className="col-xs-6">
                                                    <p className="text-left">TOTAL</p>
                                                </div>
                                                <div className="col-xs-6">
                                                    <p className="text-right">$50</p>
                                                </div>
                                            </div>

                                            <div className="row">
                                                <div className="col-xs-12">
                                                    <p className="text-left">Have a <a style={{color: '#fe8fb7', cursor: 'pointer', textDecoration: 'underline'}}>coupon code</a>?</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row emailNoteContainerWeb onlyWeb">
                                    <div className="col-xs-12">
                                        <img className="orderImg" src={require("./client/images/order.png")} alt="order" />
                                        <p>
                                            You will receive an email confirmation when recipient accepts your gift.
                                            Scentbird ships between the 15th and the 18th of every month. Recipient will
                                            receive an email confirmation of shipment every month. Please allow 5-7 days for delivery.
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xs-12 col-sm-8 rightSideContainer">
                                <div className="row">
                                    <div className="col-xs-12 subscriptionType onlyMobile">
                                        <p>
                                            Choose your subscription type
                                        </p>
                                        <div className="row">
                                            <div className="col-xs-6 gender">
                                                <img src={require("./client/images/female.png")} alt="female" name="female" onClick={this.checkGenderByImage.bind(this)} />
                                                <div className="genderCheckBox">
                                                    <RadioGroup
                                                            aria-label="gender"
                                                            name="gender2"
                                                            className={classes.group}
                                                            value={this.state.gender}
                                                            flex-direction="column"
                                                            onChange={this.checkGender.bind(this)}
                                                        >
                                                        <FormControlLabel
                                                            value="female"
                                                            control={<Radio color="primary" />}
                                                            label="For women"
                                                            className={classes.checkboxLabel}
                                                            classes={{
                                                                label: classes.checkboxLabel
                                                            }}
                                                            labelPlacement="start"
                                                        />
                                                    </RadioGroup>
                                                </div>
                                            </div>

                                            <div className="col-xs-6 gender">
                                                <img src={require("./client/images/male.png")} alt="male" name="male" onClick={this.checkGenderByImage.bind(this)} />
                                                <div className="genderCheckBox">
                                                    <RadioGroup
                                                        aria-label="gender"
                                                        name="gender2"
                                                        className={classes.group}
                                                        value={this.state.gender}
                                                        flex-direction="column"
                                                        onChange={this.checkGender.bind(this)}
                                                    >
                                                        <FormControlLabel
                                                            value="male"
                                                            control={<Radio color="primary" />}
                                                            label="For men"
                                                            labelPlacement="start"
                                                            className={classes.checkboxLabel}
                                                            classes={{
                                                                label: classes.checkboxLabel
                                                            }}
                                                        />
                                                    </RadioGroup>
                                                </div>
                                            </div>
                                        </div>
                                        <p className="genderError">{this.state.genderError}</p>
                                    </div>

                                    <div className="col-xs-12 monthlySubscriptionWeb onlyWeb">
                                        <h1 className="noMargin">MONTH-TO-MONTH SUBSCRIPTION</h1>
                                        <h5 style={{color: '#ff67a1'}}>Billed monthly. Renews automatically, cancel any time. Free shipping.</h5>
                                    </div>

                                    <div className="col-xs-12">
                                        <div className="row">
                                            <div className="onlyWeb">
                                                <div className="col-xs-12 inputLabel">
                                                    <label htmlFor="Create account">Create account</label>
                                                </div>

                                                <div className="col-sm-6">
                                                    <TextField
                                                        id="emailAddress"
                                                        label="Email address"
                                                        margin="normal"
                                                        name="email"
                                                        helperText={this.state.emailAddressError}
                                                        className={classes.textField}
                                                        onChange={this.validate.bind(this)}
                                                        onBlur={this.validate.bind(this)}
                                                        inputRef={r => this.emailAddress = r}
                                                        FormHelperTextProps={{className: classes.formHelper}}
                                                        InputProps={{
                                                            disableUnderline: true,
                                                            className: 'inputFieldWrapper',
                                                            classes: {
                                                                input: classes.inputField,
                                                            },
                                                        }}
                                                        InputLabelProps={{
                                                            FormLabelClasses: {
                                                                root: classes.inputLabel,
                                                                focused: classes.inputLabelFocused,
                                                                filled: classes.inputLabelFilled,
                                                            }
                                                        }}
                                                    />
                                                </div>

                                                <div className="col-sm-6 onlyWeb">
                                                    <TextField
                                                        id="password"
                                                        label="Password"
                                                        type="password"
                                                        margin="normal"
                                                        name="password"
                                                        helperText={this.state.passwordError}
                                                        className={classes.textField}
                                                        onChange={this.validate.bind(this)}
                                                        onBlur={this.validate.bind(this)}
                                                        inputRef={r => this.password = r}
                                                        FormHelperTextProps={{className: classes.formHelper}}
                                                        InputProps={{
                                                            disableUnderline: true,
                                                            className: 'inputFieldWrapper',
                                                            classes: {
                                                                input: classes.inputField,
                                                            },
                                                        }}
                                                        InputLabelProps={{
                                                            FormLabelClasses: {
                                                                root: classes.inputLabel,
                                                                focused: classes.inputLabelFocused,
                                                                filled: classes.inputLabelFilled,
                                                            }
                                                        }}
                                                    />
                                                </div>
                                            </div>

                                            <div className="col-xs-12 inputLabel">
                                                <label>Shipping address</label>
                                            </div>

                                            <div className="col-xs-12 col-sm-6">
                                                <TextField
                                                    id="firstName"
                                                    label="First name"
                                                    margin="normal"
                                                    name="firstName"
                                                    helperText={this.state.firstNameError}
                                                    className={classes.textField}
                                                    onChange={this.validate.bind(this)}
                                                    onBlur={this.validate.bind(this)}
                                                    inputRef={r => this.firstName = r}
                                                    FormHelperTextProps={{className: classes.formHelper}}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        className: 'inputFieldWrapper',
                                                        classes: {
                                                            input: classes.inputField,
                                                        },
                                                    }}
                                                    InputLabelProps={{
                                                        FormLabelClasses: {
                                                            root: classes.inputLabel,
                                                            focused: classes.inputLabelFocused,
                                                            filled: classes.inputLabelFilled,
                                                        }
                                                    }}
                                                />
                                            </div>

                                            <div className="col-xs-12 col-sm-6">
                                                <TextField
                                                    id="lastName"
                                                    label="Last name"
                                                    margin="normal"
                                                    name="lastName"
                                                    helperText={this.state.lastNameError}
                                                    className={classes.textField}
                                                    onChange={this.validate.bind(this)}
                                                    onBlur={this.validate.bind(this)}
                                                    inputRef={r => this.lastName = r}
                                                    FormHelperTextProps={{className: classes.formHelper}}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        className: 'inputFieldWrapper',
                                                        classes: {
                                                            input: classes.inputField,
                                                        },
                                                    }}
                                                    InputLabelProps={{
                                                        FormLabelClasses: {
                                                            root: classes.inputLabel,
                                                            focused: classes.inputLabelFocused,
                                                            filled: classes.inputLabelFilled,
                                                        }
                                                    }}
                                                />
                                            </div>

                                            <div className="col-xs-12 col-sm-8">
                                                <TextField
                                                    id="streetAddress"
                                                    label="Street address"
                                                    margin="normal"
                                                    name="streetAdress"
                                                    helperText={this.state.streetAddressError}
                                                    className={classes.textField}
                                                    onChange={this.validate.bind(this)}
                                                    onBlur={this.validate.bind(this)}
                                                    inputRef={r => this.streetAddress = r}
                                                    FormHelperTextProps={{className: classes.formHelper}}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        className: 'inputFieldWrapper',
                                                        classes: {
                                                            input: classes.inputField,
                                                        },
                                                    }}
                                                    InputLabelProps={{
                                                        FormLabelClasses: {
                                                            root: classes.inputLabel,
                                                            focused: classes.inputLabelFocused,
                                                            filled: classes.inputLabelFilled,
                                                        }
                                                    }}
                                                />
                                            </div>

                                            <div className="col-xs-12 col-sm-4">
                                                <TextField
                                                    id="aptSuite"
                                                    label="Apt/Suite (Optional)"
                                                    margin="normal"
                                                    className={classes.textField}
                                                    onChange={this.validate.bind(this)}
                                                    inputRef={r => this.aptSuite = r}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        className: 'inputFieldWrapper',
                                                        classes: {
                                                            input: classes.inputField,
                                                        },
                                                    }}
                                                    InputLabelProps={{
                                                        FormLabelClasses: {
                                                            root: classes.inputLabel,
                                                            focused: classes.inputLabelFocused,
                                                            filled: classes.inputLabelFilled,
                                                        }
                                                    }}
                                                />
                                            </div>

                                            <div className="col-xs-12 col-sm-4">
                                                <TextField
                                                    id="zipCode"
                                                    label="Zip code"
                                                    margin="normal"
                                                    name="zipCode"
                                                    helperText={this.state.zipCodeError}
                                                    className={classes.textField}
                                                    onChange={this.validate.bind(this)}
                                                    onBlur={this.validate.bind(this)}
                                                    inputRef={r => this.zipCode = r}
                                                    FormHelperTextProps={{className: classes.formHelper}}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        className: 'inputFieldWrapper',
                                                        classes: {
                                                            input: classes.inputField,
                                                        },
                                                    }}
                                                    InputLabelProps={{
                                                        FormLabelClasses: {
                                                            root: classes.inputLabel,
                                                            focused: classes.inputLabelFocused,
                                                            filled: classes.inputLabelFilled,
                                                        }
                                                    }}
                                                />
                                            </div>

                                            <div className="col-xs-12 col-sm-4">
                                                <TextField
                                                    id="region"
                                                    select
                                                    label="State"
                                                    name="region"
                                                    disabled={this.state.disableRegion}
                                                    className={classes.textField}
                                                    value={this.state.region}
                                                    onChange={this.selectHandler.bind(this)}
                                                    onBlur={this.validateSelect.bind(this)}
                                                    helperText={this.state.regionError}
                                                    FormHelperTextProps={{className: classes.formHelper}}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        classes: {
                                                            input: classes.inputFieldSelect,
                                                        },
                                                    }}
                                                    InputLabelProps={{
                                                        FormLabelClasses: {
                                                            root: classes.inputLabel,
                                                            focused: classes.inputLabelFocused,
                                                            filled: classes.inputLabelFilled,
                                                        }
                                                    }}
                                                    SelectProps={{
                                                        classes: {
                                                            root: classes.selectFields,
                                                            select: classes.select,
                                                        },
                                                    }}
                                                    margin="normal"
                                                >
                                                    {regions.map(option => (
                                                        <MenuItem key={option} value={option}>
                                                            {option}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>

                                            <div className="col-xs-12 col-sm-4">
                                                <TextField
                                                    id="city"
                                                    label="City"
                                                    margin="normal"
                                                    name="city"
                                                    helperText={this.state.cityError}
                                                    className={classes.textField}
                                                    onChange={this.validate.bind(this)}
                                                    onBlur={this.validate.bind(this)}
                                                    inputRef={r => this.city = r}
                                                    FormHelperTextProps={{className: classes.formHelper}}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        className: 'inputFieldWrapper',
                                                        classes: {
                                                            input: classes.inputField,
                                                        },
                                                    }}
                                                    InputLabelProps={{
                                                        FormLabelClasses: {
                                                            root: classes.inputLabel,
                                                            focused: classes.inputLabelFocused,
                                                            filled: classes.inputLabelFilled,
                                                        }
                                                    }}
                                                />
                                            </div>

                                            <div className="col-xs-12 col-sm-12">
                                                <TextField
                                                    id="country"
                                                    select
                                                    label="Country"
                                                    name="country"
                                                    className={classes.textField}
                                                    value={this.state.country}
                                                    onChange={this.selectHandler.bind(this)}
                                                    onBlur={this.validateSelect.bind(this)}
                                                    helperText={this.state.countryError}
                                                    FormHelperTextProps={{className: classes.formHelper}}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        classes: {
                                                            input: classes.inputFieldSelect,
                                                        },
                                                    }}
                                                    InputLabelProps={{
                                                        FormLabelClasses: {
                                                            root: classes.inputLabel,
                                                            focused: classes.inputLabelFocused,
                                                            filled: classes.inputLabelFilled,
                                                        }
                                                    }}
                                                    SelectProps={{
                                                        classes: {
                                                            root: classes.selectFields,
                                                            select: classes.select,
                                                        },
                                                    }}
                                                    margin="normal"
                                                >
                                                    {countries.map(option => (
                                                        <MenuItem key={option.country} value={option.country}>
                                                            {option.country}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>

                                            <div className="col-xs-12 col-sm-6">
                                                <TextField
                                                    id="mobile"
                                                    label="Mobile number (Optional)"
                                                    margin="normal"
                                                    className={classes.textField}
                                                    onChange={this.validate.bind(this)}
                                                    inputRef={r => this.mobile = r}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        classes: {
                                                            input: classes.inputField,
                                                        },
                                                    }}
                                                    InputLabelProps={{
                                                        FormLabelClasses: {
                                                            root: classes.inputLabel,
                                                            focused: classes.inputLabelFocused,
                                                            filled: classes.inputLabelFilled,
                                                        }
                                                    }}
                                                />
                                            </div>

                                            <div className="col-sm-6 onlyWeb">
                                                <div className="discountsText">
                                                    We may send you special discount and offers
                                                </div>
                                            </div>

                                            <div className="col-xs-12 col-sm-12">
                                                <FormGroup row>
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox
                                                                checked={this.state.billingAddressCheck}
                                                                onChange={this.handleCheck('billingAddressCheck')}
                                                                value="billingAddressCheck"
                                                                classes={{
                                                                    root: classes.billingRoot,
                                                                    checked: classes.checked,
                                                                }}
                                                            />
                                                        }
                                                        label="Use this address as my billing address"
                                                        classes={{
                                                            label: classes.regularLabel
                                                        }}
                                                    />
                                                </FormGroup>
                                            </div>

                                            {this.state.showBillingAddress && <div>
                                                <div className="col-xs-12 inputLabel">
                                                    <label>Billing Address</label>
                                                </div>

                                                <div className="col-xs-12 col-sm-8">
                                                    <TextField
                                                        id="billingStreetAddress"
                                                        label="Street address"
                                                        margin="normal"
                                                        name="billingStreetAddress"
                                                        helperText={this.state.billingStreetAddressError}
                                                        className={classes.textField}
                                                        onChange={this.validate.bind(this)}
                                                        onBlur={this.validate.bind(this)}
                                                        inputRef={r => this.billingStreetAddress = r}
                                                        FormHelperTextProps={{className: classes.formHelper}}
                                                        InputProps={{
                                                            disableUnderline: true,
                                                            className: 'inputFieldWrapper',
                                                            classes: {
                                                                input: classes.inputField,
                                                            },
                                                        }}
                                                        InputLabelProps={{
                                                            FormLabelClasses: {
                                                                root: classes.inputLabel,
                                                                focused: classes.inputLabelFocused,
                                                                filled: classes.inputLabelFilled,
                                                            }
                                                        }}
                                                    />
                                                </div>

                                                <div className="col-xs-12 col-sm-4">
                                                    <TextField
                                                        id="billingAptSuite"
                                                        label="Apt/Suite (Optional)"
                                                        margin="normal"
                                                        className={classes.textField}
                                                        onChange={this.validate.bind(this)}
                                                        inputRef={r => this.billingAptSuite = r}
                                                        InputProps={{
                                                            disableUnderline: true,
                                                            className: 'inputFieldWrapper',
                                                            classes: {
                                                                input: classes.inputField,
                                                            },
                                                        }}
                                                        InputLabelProps={{
                                                            FormLabelClasses: {
                                                                root: classes.inputLabel,
                                                                focused: classes.inputLabelFocused,
                                                                filled: classes.inputLabelFilled,
                                                            }
                                                        }}
                                                    />
                                                </div>

                                                <div className="col-xs-12 col-sm-4">
                                                    <TextField
                                                        id="billingZipCode"
                                                        label="Zip code"
                                                        margin="normal"
                                                        name="billingZipCode"
                                                        helperText={this.state.billingZipCodeError}
                                                        className={classes.textField}
                                                        onChange={this.validate.bind(this)}
                                                        onBlur={this.validate.bind(this)}
                                                        inputRef={r => this.billingZipCode = r}
                                                        FormHelperTextProps={{className: classes.formHelper}}
                                                        InputProps={{
                                                            disableUnderline: true,
                                                            className: 'inputFieldWrapper',
                                                            classes: {
                                                                input: classes.inputField,
                                                            },
                                                        }}
                                                        InputLabelProps={{
                                                            FormLabelClasses: {
                                                                root: classes.inputLabel,
                                                                focused: classes.inputLabelFocused,
                                                                filled: classes.inputLabelFilled,
                                                            }
                                                        }}
                                                    />
                                                </div>

                                                <div className="col-xs-12 col-sm-4">
                                                    <TextField
                                                        id="billingRegion"
                                                        select
                                                        label="State"
                                                        name="billingRegion"
                                                        disabled={this.state.disableBillingRegion}
                                                        className={classes.textField}
                                                        value={this.state.billingRegion}
                                                        onChange={this.selectHandler.bind(this)}
                                                        onBlur={this.validateSelect.bind(this)}
                                                        helperText={this.state.billingRegionError}
                                                        FormHelperTextProps={{className: classes.formHelper}}
                                                        InputProps={{
                                                            disableUnderline: true,
                                                            classes: {
                                                                input: classes.inputFieldSelect,
                                                            },
                                                        }}
                                                        InputLabelProps={{
                                                            FormLabelClasses: {
                                                                root: classes.inputLabel,
                                                                focused: classes.inputLabelFocused,
                                                                filled: classes.inputLabelFilled,
                                                            }
                                                        }}
                                                        SelectProps={{
                                                            classes: {
                                                                root: classes.selectFields,
                                                                select: classes.select,
                                                            },
                                                        }}
                                                        margin="normal"
                                                    >
                                                        {billingRegions.map(option => (
                                                            <MenuItem key={option} value={option}>
                                                                {option}
                                                            </MenuItem>
                                                        ))}
                                                    </TextField>
                                                </div>

                                                <div className="col-xs-12 col-sm-4">
                                                    <TextField
                                                        id="billingCity"
                                                        label="City"
                                                        margin="normal"
                                                        name="billingCity"
                                                        helperText={this.state.billingCityError}
                                                        className={classes.textField}
                                                        onChange={this.validate.bind(this)}
                                                        onBlur={this.validate.bind(this)}
                                                        inputRef={r => this.billingCity = r}
                                                        FormHelperTextProps={{className: classes.formHelper}}
                                                        InputProps={{
                                                            disableUnderline: true,
                                                            className: 'inputFieldWrapper',
                                                            classes: {
                                                                input: classes.inputField,
                                                            },
                                                        }}
                                                        InputLabelProps={{
                                                            FormLabelClasses: {
                                                                root: classes.inputLabel,
                                                                focused: classes.inputLabelFocused,
                                                                filled: classes.inputLabelFilled,
                                                            }
                                                        }}
                                                    />
                                                </div>

                                                <div className="col-xs-12 col-sm-12">
                                                    <TextField
                                                        id="billingCountry"
                                                        select
                                                        label="Country"
                                                        name="billingCountry"
                                                        className={classes.textField}
                                                        value={this.state.billingCountry}
                                                        onChange={this.selectHandler.bind(this)}
                                                        onBlur={this.validateSelect.bind(this)}
                                                        helperText={this.state.billingCountryError}
                                                        FormHelperTextProps={{className: classes.formHelper}}
                                                        InputProps={{
                                                            disableUnderline: true,
                                                            classes: {
                                                                input: classes.inputFieldSelect,
                                                            },
                                                        }}
                                                        InputLabelProps={{
                                                            FormLabelClasses: {
                                                                root: classes.inputLabel,
                                                                focused: classes.inputLabelFocused,
                                                                filled: classes.inputLabelFilled,
                                                            }
                                                        }}
                                                        SelectProps={{
                                                            classes: {
                                                                root: classes.selectFields,
                                                                select: classes.select,
                                                            },
                                                        }}
                                                        margin="normal"
                                                    >
                                                        {countries.map(option => (
                                                            <MenuItem key={option.country} value={option.country}>
                                                                {option.country}
                                                            </MenuItem>
                                                        ))}
                                                    </TextField>
                                                </div>
                                            </div>}

                                            <div className="col-xs-12 inputLabel">
                                                <label>Secure credit card payment</label>
                                            </div>

                                            <div className="col-xs-12 creditCardSecure">

                                                <div>
                                                    <div className="col-xs-12 col-sm-8">
                                                        <img className="lockImg" src={require("./client/images/lock.png")} alt="lock" />
                                                        <span style={{color: '#99c490'}}>128-BIT ENCRYPTION. YOU'RE SAFE</span>
                                                    </div>

                                                    <div className="col-xs-12 col-sm-4">
                                                        <div className="cardContainer">
                                                            <img className={'card' + (this.state.selectedCardType === 'visa' ? ' selected' : '')} src={require("./client/images/visa.png")} alt="visa" />
                                                            <img className={'card' + (this.state.selectedCardType === 'master' ? ' selected' : '')} src={require("./client/images/master.png")} alt="master" />
                                                            <img className={'card' + (this.state.selectedCardType === 'discover' ? ' selected' : '')} src={require("./client/images/discover.png")} alt="discover" />
                                                            <img className={'card' + (this.state.selectedCardType === 'AMEX' ? ' selected' : '')} src={require("./client/images/amex.png")} alt="american" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="col-xs-12 col-sm-9">
                                                    <TextField
                                                        id="creditCardNumber"
                                                        label="Credit card number"
                                                        margin="normal"
                                                        name="creditCardNumber"
                                                        helperText={this.state.creditCardNumberError}
                                                        className={classes.textField}
                                                        onChange={this.validateCreditCard.bind(this)}
                                                        onBlur={this.validateCreditCard.bind(this)}
                                                        inputRef={r => this.creditCardNumber = r}
                                                        FormHelperTextProps={{className: classes.formHelper}}
                                                        InputProps={{
                                                            disableUnderline: true,
                                                            className: 'inputFieldWrapper',
                                                            classes: {
                                                                input: classes.inputField,
                                                            },
                                                        }}
                                                        InputLabelProps={{
                                                            FormLabelClasses: {
                                                                root: classes.inputLabel,
                                                                focused: classes.inputLabelFocused,
                                                                filled: classes.inputLabelFilled,
                                                            }
                                                        }}
                                                    />
                                                </div>

                                                <div className="col-xs-6 col-sm-3 onlyWeb">
                                                    <TextField
                                                        id="securityCode"
                                                        label="Security code"
                                                        margin="normal"
                                                        name="securityCode"
                                                        type="password"
                                                        helperText={this.state.securityCodeError}
                                                        className={classes.textField}
                                                        onChange={this.validateSecurityCode.bind(this)}
                                                        onBlur={this.validateSecurityCode.bind(this)}
                                                        inputRef={r => this.securityCode = r}
                                                        FormHelperTextProps={{className: classes.formHelper}}
                                                        InputProps={{
                                                            disableUnderline: true,
                                                            className: 'inputFieldWrapper',
                                                            classes: {
                                                                input: classes.inputField,
                                                            },
                                                        }}
                                                        InputLabelProps={{
                                                            FormLabelClasses: {
                                                                root: classes.inputLabel,
                                                                focused: classes.inputLabelFocused,
                                                                filled: classes.inputLabelFilled,
                                                            }
                                                        }}
                                                    />
                                                </div>

                                                <div className="col-xs-6">
                                                    <TextField
                                                        id="month"
                                                        select
                                                        label="Month"
                                                        name="month"
                                                        className={classes.textField}
                                                        value={this.state.month}
                                                        onChange={this.selectHandler.bind(this)}
                                                        onBlur={this.validateSelect.bind(this)}
                                                        helperText={this.state.monthError}
                                                        FormHelperTextProps={{className: classes.formHelper}}
                                                        InputProps={{
                                                            disableUnderline: true,
                                                            classes: {
                                                                input: classes.inputFieldSelect,
                                                            },
                                                        }}
                                                        InputLabelProps={{
                                                            FormLabelClasses: {
                                                                root: classes.inputLabel,
                                                                focused: classes.inputLabelFocused,
                                                                filled: classes.inputLabelFilled,
                                                            }
                                                        }}
                                                        SelectProps={{
                                                            classes: {
                                                                root: classes.selectFields,
                                                                select: classes.select,
                                                            },
                                                        }}
                                                        margin="normal"
                                                    >
                                                        {months.map(option => (
                                                            <MenuItem key={option.name} value={option.name}>
                                                                {option.name}
                                                            </MenuItem>
                                                        ))}
                                                    </TextField>
                                                </div>

                                                <div className="col-xs-6">
                                                    <TextField
                                                        id="year"
                                                        select
                                                        label="Year"
                                                        name="year"
                                                        className={classes.textField}
                                                        value={this.state.year}
                                                        onChange={this.selectHandler.bind(this)}
                                                        onBlur={this.validateSelect.bind(this)}
                                                        helperText={this.state.yearError}
                                                        FormHelperTextProps={{className: classes.formHelper}}
                                                        InputProps={{
                                                            disableUnderline: true,
                                                            classes: {
                                                                input: classes.inputFieldSelect,
                                                            },
                                                        }}
                                                        InputLabelProps={{
                                                            FormLabelClasses: {
                                                                root: classes.inputLabel,
                                                                focused: classes.inputLabelFocused,
                                                                filled: classes.inputLabelFilled,
                                                            }
                                                        }}
                                                        SelectProps={{
                                                            classes: {
                                                                root: classes.selectFields,
                                                                select: classes.select,
                                                            },
                                                        }}
                                                        margin="normal"
                                                    >
                                                        {years.map(option => (
                                                            <MenuItem key={option} value={option}>
                                                                {option}
                                                            </MenuItem>
                                                        ))}
                                                    </TextField>
                                                </div>

                                                <div className="col-xs-12 col-sm-3 onlyMobile">
                                                    <TextField
                                                        id="securityCodeMobile"
                                                        label="Security code"
                                                        margin="normal"
                                                        name="securityCodeMobile"
                                                        helperText={this.state.securityCodeMobileError}
                                                        className={classes.textField}
                                                        onChange={this.validate.bind(this)}
                                                        onBlur={this.validate.bind(this)}
                                                        inputRef={r => this.securityCodeMobile = r}
                                                        FormHelperTextProps={{className: classes.formHelper}}
                                                        InputProps={{
                                                            disableUnderline: true,
                                                            className: 'inputFieldWrapper',
                                                            classes: {
                                                                input: classes.inputField,
                                                            },
                                                        }}
                                                        InputLabelProps={{
                                                            FormLabelClasses: {
                                                                root: classes.inputLabel,
                                                                focused: classes.inputLabelFocused,
                                                                filled: classes.inputLabelFilled,
                                                            }
                                                        }}
                                                    />
                                                </div>
                                            </div>

                                            <div className="clearfix"></div>
                                            <div className="actionsContainer">
                                                <Button variant="contained" color="primary" onClick={this.register.bind(this)} className="buyNow">
                                                    BUY NOW
                                                    <Icon className={classes.rightIcon}>arrow_forward</Icon>
                                                </Button>
                                                <a href="#" className="backLink onlyWeb">Back</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <div className="col-xs-12 emailNoteContainerMobile onlyMobile">
                                            <p>
                                                You will receive an email confirmation when recipient accepts your gift.
                                                Scentbird ships between the 15th and the 18th of every month. Recipient will
                                                receive an email confirmation of shipment every month. Please allow 5-7 days for delivery.
                                            </p>
                                            <img className="orderImg" src={require("./client/images/order.png")} alt="order" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(App);
