/**
 * Created by vahe on 8/19/18.
 */

const styles = theme => ({
    creditRoot: {
        borderRadius: 0,
        color: "#979797",
        width: 18,
        height: 18,
        "&$checked": {
            color: "#ff6ba6",
            backgroundColor: "#ffffff",
            borderRadius: 0,
            width: 18,
            height: 18,
        }
    },
    billingRoot: {
        borderRadius: 0,
        color: "#979797",
        "&$checked": {
            color: "#ff6ba6",
            backgroundColor: "#ffffff",
            borderRadius: 0,
        }
    },
    checked: {},
    textField: {
        width: '100%',
        fontSize: 25,
        margin: 4,
    },
    icon: {
        margin: theme.spacing.unit,
        fontSize: 32,
    },
    selectFields: {
        border: '1px solid #e6e6e6',
        cursor: 'pointer',
    },
    select: {
        padding: 0,
    },
    inputField: {
        border: '1px solid #E6E6E6',
        padding: 14,
        fontFamily: 'ProximaNovaRegular',
        fontSize: 16,
    },
    inputFieldSelect: {
        padding: '19px 14px 10px 16px',
        fontFamily: 'ProximaNovaRegular',
        fontSize: 16
    },
    inputLabel: {
        fontFamily: 'ProximaNovaRegular',
        fontSize: 16,
        left: 10,
        top: 9,
    },
    inputLabelFocused: {
        fontFamily: 'ProximaNovaRegular',
        fontSize: 16,
        left: 10,
        top: 0,
        color: '#9B9B9B !important'
    },
    inputLabelFilled: {
        top: 0
    },
    formHelper: {
        color: 'red'
    },
    group: {
        margin: `${theme.spacing.unit}px 0`,
        display: "block",
        position: 'absolute',
        top: 70,
        left: 100,
    },
    checkboxLabel: {
        flexDirection: 'column',
        fontSize: 14,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
        position: 'absolute',
        right: 20
    },
    regularLabel: {
        fontSize: 12
    }
});

export default styles