/**
 * Created by vahe on 8/19/18.
 */

const months = [
    {
        "name": "01 - January"
    },
    {
        "name": "02 - February"
    },
    {
        "name": "03 - March"
    },
    {
        "name": "04 - April"
    },
    {
        "name": "05 - May"
    },
    {
        "name": "06 - June"
    },
    {
        "name": "07 - July"
    },
    {
        "name": "08 - August"
    },
    {
        "name": "09 - September"
    },
    {
        "name": "10 - October"
    },
    {
        "name": "11 - November"
    },
    {
        "name": "12 - December"
    }
];

export default months;