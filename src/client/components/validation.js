/**
 * Created by vahe on 8/19/18.
 */

import _ from 'underscore';
import countries from './countries';

export default class Validation {

    static SecurityCodeValidation(e, self) {
        let value = e.target.value;
        let ref = e.target.id;

        let isCorrect = value !== '111';
        let borderColor = isCorrect ? '#e6e6e6' : '#fd6464';
        let errorMessage = isCorrect ? '' : 'Invalid Security Code';

        self[ref].style.border = '1px solid ' + borderColor;
        self.setState({
            'securityCodeError': errorMessage,
        });
    }

    static CreditCardValidation(e, self) {
        let cardRules = [
            {
                type: 'visa',
                pattern: '^4',
            },
            {
                type: 'master',
                pattern: '^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$',
            },
            {
                type: 'AMEX',
                pattern: '^3[47]',
            },
            {
                type: 'discover',
                pattern: '^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)',
            }
        ];
        let value = e.target.value;
        let ref = e.target.id;
        let result = null;
        cardRules.map(rule => {
            if (new RegExp(rule.pattern).test(value)) {
                result = rule.type;
            }
        });
        self.setState({
            'selectedCardType': result,
        });
        let commonCardRegexp = new RegExp('^[0-9]{16}$');
        let correctNumber = result && commonCardRegexp.test(value);
        let borderColor = correctNumber ? '#e6e6e6' : '#fd6464';
        let errorMessage = correctNumber ? '' : 'Invalid Card Number';

        self[ref].style.border = '1px solid ' + borderColor;
        self.setState({
            'creditCardNumberError': errorMessage,
        });
    }

    static InputValidation (e, self) {
        let value = e.target.value;
        let ref = e.target.id;
        let errorMessage = '';

        if (value && value.length) {
            if (e.target.name === 'email' && !(value && /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(value)) ) {
                errorMessage = 'Invalid Email Address';
            } else if (e.target.name === 'password' && value.length < 10) {
                errorMessage = 'Password is short';
            }
        } else {
            errorMessage = 'This field is required';
        }

        let borderColor = errorMessage && errorMessage.length ? '#fd6464' : '#e6e6e6';
        self[ref].style.border = '1px solid ' + borderColor;

        self.setState({
            [ref+'Error']: errorMessage,
        });
    }

    static SelectValidation (e, self) {
        let errorMessage = '';
        let value = e.target.value;
        let name = e.target.name;
        let isAddress = (name === 'region' || name === 'billingRegion') ? true : false;


        if (!value) {
            let states = isAddress && _.findWhere(countries, {country: self.state.country})['states'];
            errorMessage = 'This field is required';

            if (isAddress && !states.length) {
                errorMessage = '';
            }
        }

        e.nativeEvent.path[1].style.border = errorMessage.length ? '1px solid #fd6464' : '1px solid #e6e6e6';

        self.setState({
            [name+'Error']: errorMessage,
        });
    }
}
