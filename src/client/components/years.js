/**
 * Created by vahe on 8/19/18.
 */

const years = getYearsTimeRange();

function getYearsTimeRange() {
    let years = [];
    let year = new Date().getFullYear();

    for (let i = 0; i < 30; i++) {
        years.push(year);
        year++;
    }

    return years;
}

export default years;